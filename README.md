# README #

Repository for a minimal working example of the Python Time Series Analysis Shell (PyTS). Created by Maxwell Sherman in collaboration with Shane Lee for the Jones Computational Neuroscience Lab at Brown University.

### Get Started ###

* Pull the repository
* Read *user_guide.pdf* in the **/docs** directory.

### Contacts ###

Questions? Contact Maxwell Sherman at:

* msherman997@gmail.com
* 781-820-0973