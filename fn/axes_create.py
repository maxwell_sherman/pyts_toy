# axes_create.py - simple axis creation
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (Routines by SL and MS)

# Standard Modules
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import ticker

# Base figure class
# Class by SL. Some rountines by MS.
class FigBase():
    def __init__(self):
        # self.f is typically set by the super class
        self.f = None
        mpl.rc('text', usetex=True)

        # axis dicts are guaranteed to exist at least, sheesh
        self.ax = {}

    # function to set the scientific notation limits
    def set_notation_scientific(self, list_ax_handles):
        # set the formatter
        fmt = ticker.ScalarFormatter()
        fmt.set_powerlimits((-3, 3))
        for h in list_ax_handles:
            self.ax[h].yaxis.set_major_formatter(fmt)

        return fmt

    # set the font size globally
    def set_fontsize(self, s):
        font_prop = {
            'size': s,
        }

        mpl.rc('font', **font_prop)

    # generic save png function to file_name at dpi=dpi_set
    def savepng(self, file_name, dpi_set=300):
        self.f.savefig(file_name, dpi=dpi_set)

    # generic save, works for png but supposed to be for eps
    def saveeps(self, deps, fprefix):
        fname = os.path.join(deps, fprefix+'.eps')
        self.f.savefig(fname)

    # obligatory close function
    def close(self):
        plt.close(self.f)

# Simple one axis window
# By SL. Modified by MS.
class FigStd(FigBase):
    def __init__(self):
        self.f = plt.figure(figsize=(12, 6))
        self.set_fontsize(22)
        self.ax = {}

        gs0 = gridspec.GridSpec(1, 1, bottom=0.11, top=0.95)
        self.ax['ax0'] = self.f.add_subplot(gs0[:])

    def save(self, file_name):
        self.f.savefig(file_name)

# Spectrogram plus time series
# By MS
class FigSpec(FigBase):
    def __init__(self):
        self.f = plt.figure(figsize=(12, 12))
        self.set_fontsize(22)

        # the right margin is a hack and NOT guaranteed!
        # it's making space for the stupid colorbar that creates a new grid to replace gs1
        self.gspec = {
            'dpl': gridspec.GridSpec(2, 1, height_ratios=[1, 3], bottom=0.555, top=0.975, left=0.1, right=0.82),
            'spec': gridspec.GridSpec(1, 4, wspace=0.05, hspace=0., bottom=0.055, top=0.475, left=0.1, right=1.),
        }

        self.ax = {}
        self.ax['dipole'] = self.f.add_subplot(self.gspec['dpl'][:, :])
        self.ax['spec'] = self.f.add_subplot(self.gspec['spec'][:, :])
