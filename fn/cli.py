# cli.py - routines for the command line interface console data.py
#
# v 0.0.1
# rev 2015-10-05: (MS: Created for Center for Open Science)
# Notes: (some routines by MS. Some by SL)

# Standard Modules
from cmd import Cmd
import os, traceback
import numpy as np

# Local Modules
import clidefs
import fileio as fio
import paramrw

class Console(Cmd):
    # By MS with guidance from SL
    def __init__(self):
        Cmd.__init__(self)
        print "\nThis is the time-series analysis Shell"

        self.prompt = '\033[93m' + "[datash] " + '\033[0m'
        self.intro  = ""
        self.f_history = '.data_hist_local'

        # Initialize some important attributes
        self.dirs = {}
        self.ls = {}

        # For toy model, set working directory to inlcuded data directory
        self.dirs['dproj'] = os.path.join(os.getcwd(), 'data')
        self.__update_cwd('dproj')

        # create the initial datadir list
        self.__update_ls()

        print "\nYour data directory is: %s" % self.dirs['dproj']

######### Private functions for repetative use #########

    # By MS
    def __update_cwd(self, path):
        self.dirs['cwd'] = self.dirs[path]

    # update the list of directory contents
    # By SL. Modified by MS
    def __update_ls(self):
        if os.path.exists(self.dirs['cwd']):
            self.ls['dlist'] = ['/'+d for d in os.listdir(self.dirs['cwd']) 
                                if os.path.isdir(os.path.join(self.dirs['cwd'], d))]

            self.ls['flist'] = [f for f in os.listdir(self.dirs['cwd']) 
                                if os.path.isfile(os.path.join(self.dirs['cwd'], f))]

        else:
            raise OSError("The current working directory does not exist.")

######### Executable functions in CLI #########

    # Function to set the experiment directory
    # By MS with guidance from SL
    def do_setexpt(self, args):
        """Sets the experiment within which the CLI will work.
           Usage: [datash] setexpt <experiment-name>
        """
        # If argument supplied, check that such a directory exists
        if args:
            dcheck = os.path.join(self.dirs['dproj'], args)

            if os.path.exists(dcheck):
                self.dirs['expt'] = dcheck
            else:
                raise OSError("No such directory exists. Please try again.")

        # If no argument supplied, set to current data directory
        else:
            print "You did not supply an experiment directory. Defaulting to this dir."
            self.dirs['expt'] = self.dirs['dproj']

        # update the ls contents for new directory
        self.__update_cwd('expt')
        self.__update_ls()

        print "Experiment set to", self.dirs['expt'], "\n"

    # Function to 'load' a data set within an experiment
    # Written by MS with guidance from SL
    def do_load(self, args):
        """Load data set within experiment directory. Usage example:
           Usage: [datash] load <data-set>
        """
        if args:
            dcheck = os.path.join(self.dirs['expt'], args)

        else:
            raise OSError("You didn't supply a trial directory to load. Try again.")

        # check existence of the path
        if os.path.exists(dcheck):
            self.dirs['ddata'] = dcheck
            self.__update_cwd('ddata')
            self.__update_ls()

            print "Trial directory set to: %s. \nReady for data anylsis.\n" %self.dirs['ddata']

        # If path doesn't exist, complain.
        else:
            raise OSError("Could not find %s. \nmaybe check your experiment..." %dcheck)

    # List contents of CWD
    # By SL. Modified by MS.
    def do_ls(self, args):
        """Lists contents of current working directory
           Usage: [datash] ls
        """
        if args:
            raise SyntaxError('ls takes no arguments')

        else:
            self.__update_ls()
            fio.prettyprint(self.ls['dlist'])
            fio.prettyprint(self.ls['flist'])

    # Print working dir
    # By SL. Modified by MS
    def do_pwd(self, args):
        """Displays current working directory. 
           Usage: [datash] pwd
        """
        print self.dirs['cwd']

    # Plot function
    # By MS
    def do_plot(self, args):
        """ Plots various data time-series data types (e.g. EEG, MEG, LFP)
            Usage: [datash] plot --dtype='data_type' --dir='data_dir' --xlim --runtype
            if dir not supplied, expects data to be in dir: 'raw' + opts['dtype']
        """
        opts = paramrw.create_dict_from_args(args)

        # Execute plotting fns
        if 'ts' in opts['ptype']:
            clidefs.exec_plot_ts(self.dirs['ddata'], opts)

        if 'spec' in opts['ptype']:
        # if opts['ptype'].startswith('spec'):
            clidefs.exec_plot_spec(self.dirs['ddata'], opts)

    # Averaging function
    # By MS
    def do_average(self, args):
        """Averages specified files of specified data type in specified directory
           Usage: [datash] average --dtype='dpl' --dir='rawdpl' --trials=[0, -1]
        """
        dict_opts = paramrw.create_dict_from_args(args)
        clidefs.exec_average(self.dirs['ddata'], dict_opts) 

    # Spectral Analysis
    # By MS
    def do_spec_analysis(self, args):
        """Performs spectral analysis on time-series data types (e.g. EEG, MEG, LFP)
           Usage: [datash] spec_analysis --dtype='data-type' --dir_in='data_dir' --dir_out='output_dir' --fmax --runtype
           if dir not supplied, expects data to be in dir: 'raw' + opts['dtype']
        """
        dict_opts = paramrw.create_dict_from_args(args)
        clidefs.exec_spec_analysis(self.dirs['ddata'], dict_opts)

    # Density estimation
    # By MS
    def do_density_avg(self, args):
        """Performs and plots average spectral density estimation for a data set
           Usage: [datash] density_avg --dir='data-dir'
        """
        dict_opts = paramrw.create_dict_from_args(args)
        clidefs.exec_density_avg(self.dirs['ddata'], dict_opts)

######## Command definitions to support Cmd object functionality ########
    # By SL
    def do_exit(self, args):
        """Exits from the console
        """
        return -1

    # By SL
    def do_help(self, args):
        """Get help on commands
           'help' or '?' with no arguments prints a list of commands for which help is available
           'help <command>' or '? <command>' gives help on <command>
        """
        ## The only reason to define this method is for the help text in the doc string
        Cmd.do_help(self, args)

    # Tasks before entering Cmd loop
    # By SL
    def preloop(self):
        """Initialization before prompting user for commands.
           Despite the claims in the Cmd documentaion, Cmd.preloop() is not a stub.
        """
        # Sets up command completion
        Cmd.preloop(self)

        # Load history
        self._hist = self.__load_history()

        # Initialize execution namespace for user
        self._locals  = {}
        self._globals = {}

    # Tasks exiting Cmd loop
    # By SL
    def postloop(self):
        """Take care of any unfinished business.
           Despite the claims in the Cmd documentaion, Cmd.postloop() is not a stub.
        """
        
        # Write history
        self.__write_history()

        # Clean up command completion
        Cmd.postloop(self)
        print "Exiting..."

    # Before command execution
    # By SL
    def precmd(self, line):
        """ This method is called after the line has been input but before
            it has been interpreted. If you want to modify the input line
            before execution (for example, variable substitution) do it here.
        """
        self._hist += [ line.strip() ]
        return line

    # After command execution
    # By SL
    def postcmd(self, stop, line):
        """If you want to stop the console, return something that evaluates to true.
           If you want to do some post command processing, do it here.
        """
        return stop

    # By SL
    def emptyline(self):    
        """Do nothing on empty input line"""
        pass

    # By SL
    def default(self, line):       
        """Called on an input line when the command prefix is not recognized.
           In that case we execute the line as Python code.
        """
        try:
            exec(line) in self._locals, self._globals
        except Exception, e:
            print e.__class__, ":", e

    # Function to read the history file
    # By SL
    def __load_history(self):
        with open(self.f_history) as f_in:
            lines = (line.rstrip() for line in f_in)
            lines = [line for line in lines if line]

        return lines

    # function to write the history file
    # By SL. Modified by MS
    def __write_history(self):
        with open(self.f_history, 'w') as f_out:
            for line in self._hist[-100:]:
                f_out.write(line+'\n')

    # By MS
    def cmdloop(self):
        try:
            Cmd.cmdloop(self)

        except KeyboardInterrupt:
            print "^C"
            self.cmdloop()

        except Exception, e:
            print 'Oh no, an error:'
            traceback.print_exc()
            self.cmdloop()
