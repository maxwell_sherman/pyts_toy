# ts.py - general time-series analysis functions
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (All routines by MS)

# Standard Modules
import numpy as np
import itertools as it
import os

# Local Modules
import fileio as fio
import matplotlib.pyplot as plt
import axes_create as ac

# class TimeSeries() is for a single file of time series data
# Class by MS
class TimeSeries():
    def __init__(self, f_ts, dtype='lam'):
        self.dtype = dtype

        if dtype == 'dpl':
            self.__parse_f_dpl(f_ts)

        if dtype == 'lam':
            self.__parse_f_lam(f_ts)

        # Calculate sampling freq
        self.fs = self.__calc_fs()

    def __parse_f_dpl(self, f_ts):
        x = np.loadtxt(open(f_ts, 'r'))

        # better implemented as a dict
        self.data = {
            't': x[:, 0],
            'ts': x[:, 1],
        }

        # string that holds the units
        self.units = {
            # 't': 's',
            't': 'ms',
            'ts': 'fAm',
        }

    def __parse_f_lam(self, f_ts):
        d = np.load(f_ts)

        self.data={
            't': d['t'],
            'ts': d['v'],
        }

        self.units = {
            't': 's',
            'ts': '$\mu\mathrm{V}$',
        }

    def __calc_fs(self):
        t_step = self.data['t'][1] - self.data['t'][0] 

        if self.units['t'] == 's':
            fs = 1. / t_step
        elif self.units['t'] == 'ms':
            fs = 1000. / t_step

        return fs

    # truncate data and save truncation to class
    def truncate(self, t0, T):
        self.data['t'], self.data['ts'] = self.truncate_ext(t0, T)

    # Truncate data and return the values; do not modify the class internally
    # Inclusive bounds
    def truncate_ext(self, t0, T, dtype=None):
        if dtype:
            tmask = (self.data[dtype]['t'] >= t0) & (self.data[dtype]['t'] <= T)

            t_cut = self.data[dtype]['t'][tmask]
            ts_cut = self.data[dtype]['ts'][tmask]
        
        else:
            tmask = (self.data['t'] >= t0) & (self.data['t'] <= T)

            t_cut = self.data['t'][tmask]
            ts_cut = self.data['ts'][tmask]

        return t_cut, ts_cut

    # simple plot function
    def plot(self, ax, xlim=None):
        # Truncate as necessary
        if xlim:
            t, ts = self.truncate_ext(xlim[0], xlim[1])
        else:
            t = self.data['t']
            ts = self.data['ts']

        # Plot
        ax.plot(t, ts, linewidth=2.0)

        # Return not strictly necessary...
        return ax.get_xlim()

# Assumes all ts files are the same length
# By MS
def average(fname, ts_list, dtype):
    # iterate over lists, load dpl data and average
    for fts in ts_list:
        # load ts data
        ts = TimeSeries(fts, dtype)

        # Sum the dipoles
        try:
            x_ts_agg += ts.data['ts']

        except UnboundLocalError:
            x_ts_agg = ts.data['ts']

    # poor man's mean
    x_ts_agg /= len(ts_list)

    # grab a time vector to save with avg dpl
    tvec = ts.data['t']

    # save to file
    with open(fname, 'w') as f:
        for t, x_agg in it.izip(tvec, x_ts_agg):
            f.write("%03.3f\t%5.4e\n" % (t, x_agg))

# Plot time series data
# By MS
def pts(f_ts, dfig, plot_dict, f_trials=None):
    # ts is an obj of TimeSeries() class
    ts = TimeSeries(f_ts, plot_dict['dtype'])

    # split to find file prefix
    file_prefix = f_ts.split('/')[-1].split('.')[0]

    # parse xlim from plot_dict
    if plot_dict['xlim'] is None:
        xmin = ts.data['t'][0]
        xmax = ts.data['t'][-1]

    else:
        xmin, xmax = plot_dict['xlim']

    # truncate them using logical indexing
    t_range = ts.data['t'][(ts.data['t'] >= xmin) & (ts.data['t'] <= xmax)]
    ts_range = ts.data['ts'][(ts.data['t'] >= xmin) & (ts.data['t'] <= xmax)]

    f = ac.FigStd()
    f.ax['ax0'].plot(t_range, ts_range, color='blue', linewidth=3.0)

    # set axes labels
    f.ax['ax0'].set_xlabel('Time (%s)' %ts.units['t'])
    f.ax['ax0'].set_ylabel('(%s)' %ts.units['ts'])

    # Set axis limits
    f.ax['ax0'].set_xlim([xmin, xmax])

    ylim = f.ax['ax0'].get_ylim()
    f.ax['ax0'].set_ylim([-max(np.abs(ylim)), max(np.abs(ylim))])

    # create new fig name
    fig_name = os.path.join(dfig, file_prefix+'.png')

    # savefig
    plt.savefig(fig_name, dpi=300)
    f.close()
