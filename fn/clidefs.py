# clidefs.py - these are all of the function defs for the cli
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# last major: (All functions by MS)

# Standard modules
import ast, os 
import itertools as it
import numpy as np
import multiprocessing as mp

# local modules
import fileio as fio
import paramrw
import specfn
import pspec
import tsfn

# Spectral Analysis
# By MS
def exec_spec_analysis(ddata, opts):
    p = {
        'dtype': 'dpl',
        'dir_in': 'rawdpl',
        'dir_out': 'rawspec',
        'f_max': 60,
        'runtype': 'parallel',
        't_interval': None,
    }

    paramrw.args_check(p, opts)

    # set directory to search for files in and create file name to which data will be saved
    # if opts['dir']:
    dir_in = os.path.join(ddata, p['dir_in'])
    dir_out = os.path.join(ddata, p['dir_out'])

    # check that output directory exists and if not create it
    fio.dir_create(dir_out)
    print dir_out

    # Get param file
    fparam = fio.file_match(ddata, '.param')[0]

    # Get time-series data
    file_ext = fio.get_file_ext(p['dtype'])
    list_ts = fio.file_match(dir_in, file_ext)

    # Create output file names
    if p['dtype'].endswith(('.txt', '.npz')):
        fnames_tmp = [f.split('/')[-1] for f in list_ts]
        fnames_short = [f.replace(p['dtype'], '-spec.npz') for f in fnames_tmp]
        fnames_full = [os.path.join(dir_out, f) for f in fnames_short]

    else:
        fnames_tmp = [f.split('/')[-1].split('.')[0] for f in list_ts]
        fnames_short = [f.replace(p['dtype'], 'spec') for f in fnames_tmp]
        fnames_full = [os.path.join(dir_out, f) for f in fnames_short]
        
    # Run the analysis in parallel
    if p['runtype'] == 'parallel':
        pl = mp.Pool()

        for fts, fspec in it.izip(list_ts, fnames_full):
            pl.apply_async(specfn.spec_kernel, (fparam, fts, fspec, p['f_max'], p['t_interval'], p['dtype']))

        pl.close()
        pl.join()

    # Run the analysis in serial
    elif p['runtype'] == 'debug':
        for fts, fspec in it.izip(list_ts, fnames_full):
            specfn.spec_kernel(fparam, fts, fspec, p['f_max'], p['t_interval'], p['dtype'])

# Time series plotting
# By MS
def exec_plot_ts(ddata, opts):
    # parse arg inputs
    p = {
        'ptype': 'ts',
        'dtype': 'dpl',
        'dir': None,
        'xlim': None,
        'runtype': 'parallel',
    }

    paramrw.args_check(p, opts)

    # set directory to search for files in
    fdir = fio.dir_to_search(ddata, p)

    # create fig dir name
    dir_fig = fdir.replace('raw', 'fig')
    print dir_fig

    #Check if dir exists. If not, create it.
    fio.dir_create(dir_fig)

    # Get ts files from proper directory:
    file_ext = fio.get_file_ext(p['dtype'])
    dpl_list = fio.file_match(fdir, file_ext)

    # Plot
    if p['runtype'] == 'parallel':
        pl = mp.Pool()

        for f_dpl in dpl_list:
            pl.apply_async(tsfn.pts, (f_dpl, dir_fig, p))

        pl.close()
        pl.join()

    elif p['runtype'] == 'debug':
        for f_dpl in dpl_list:
            tsfn.pts(f_dpl, dir_fig, p)

# Spectral Plotting
# By MS
def exec_plot_spec(ddata, opts):
    p = {
        'ptype': 'spec',
        'dtype': 'dpl',
        'dir': 'rawspec',
        'dir_ts': 'rawts',
        'xlim': None,
        'runtype': 'parallel',
    }

    paramrw.args_check(p, opts)

    # Set directory to search for spec files in
    fdir_spec = fio.dir_to_search(ddata, p)

    # Set directory to search for ts files in
    fdir_ts = os.path.join(ddata, p['dir_ts'])

    # Create fig dir name
    dir_fig = fdir_spec.replace('raw', 'fig')

    # Check if dir exists. If not, create it.
    fio.dir_create(dir_fig)
    print dir_fig

    # Get file ext for ts data type
    file_ext = fio.get_file_ext(p['dtype'])

    # Get spec and dpl files
    spec_list = fio.file_match(fdir_spec, '-spec.npz')
    ts_list = fio.file_match(fdir_ts, file_ext)

    # Plot
    if p['runtype'] == 'parallel':
        pl = mp.Pool()

        for f_spec, f_ts in it.izip(spec_list, ts_list):
            pl.apply_async(pspec.pspec_ts, (f_spec, f_ts, dir_fig, p))

        pl.close()
        pl.join()

    elif p['runtype'] == 'debug':
        for f_spec, f_ts in it.izip(spec_list, ts_list):
            pspec.pspec_ts(f_spec, f_ts, dir_fig, p)

# Data Averaging
# By MS
def exec_average(ddata, opts):
    p = {
        'dtype': 'dpl',
        'dir': 'rawts',
    }

    paramrw.args_check(p, opts)
 
    # Set dir to search for files in
    fdir = fio.dir_to_search(ddata, p)

    # Set output directory and ensure that it exists
    # assumes input data lives in raw data directory. Not always valid...
    dir_output = fdir.replace('raw', 'avg')
    fio.dir_create(dir_output)
    print dir_output

    # grab data files
    file_ext = fio.get_file_ext(p['dtype'])
    dlist = fio.file_match(fdir, file_ext)

    # create output file name
    prefix = dlist[0].split('/')[-1].split('-')[0]
    fname_short = '%s-average%s' %(prefix, file_ext)
    fname = os.path.join(dir_output, fname_short)

    # avg raw data
    # called fns also save data
    if 'spec' in p['dtype']:
        d_avg = specfn.average(fname, dlist)

    elif 'dpl' in p['dtype']:
        d_avg = tsfn.average(fname, dlist, p['dtype'])

# calculate average spectral density over all spec data in a given dir
# By MS
def exec_density_avg(ddata, opts):
    p = {
        'dtype': 'spec',
        'dir': 'rawspec',
        'fname': 'density_avg',
        't_interval': None,
        'f_interval': None,
    }

    paramrw.args_check(p, opts)

    # Set directory to search for spec files in
    fdir_spec = fio.dir_to_search(ddata, p)

    # Get spec and dpl files
    spec_list = fio.file_match(fdir_spec, '-spec.npz')

    # get stationary spectral density
    d_density = specfn.stationary_agg(spec_list, p['t_interval'], p['f_interval'])

    fname = os.path.join(ddata, p['fname']+'.png')
    pspec.pspecpwr(fname, d_density)
    print ddata
