# paramrw.py - routines for reading the param files and handling user input arguments
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (All functions by SL. Modified by MS)

import re, ast
import fileio as fio
import numpy as np
import itertools as it

# Reads metadata from a text file and returns as dictionary
# Does not handle lists - only ints and strings
# By SL. Modified by MS
def read(fparam):
    # Initialize dictionary
    p = {}

    # Remove empty lines from file
    lines = fio.clean_lines(fparam)

    # Read metadata line by line
    for line in lines:
        keystring, val = line.split(": ")
        key = keystring.strip()

        try:
            p[key] = float(val)
        except ValueError:
            p[key] = str(val)

    return p

# Create dictionary from user supplied inputs
# Assumes inputs of the form: --opt_1=value_1 --opt_2=value_2 ... --opt_n=value_n
# By SL
def create_dict_from_args(args):
    # split based on leading --
    args_tmp = args.split(' --')

    # only take the args that start with -- and include a =
    # drop the leading --
    args_opt = [arg for arg in args_tmp if '=' in arg]
    arg_dict = {}
    for arg in args_opt:
        # getting rid of first case, ugh, hack!
        if arg.startswith('--'):
            args_tmp = arg[2:].split('=')
            arg_dict[args_tmp[0]] = args_tmp[1]

        else:
            args_tmp = arg.split('=')
            arg_dict[args_tmp[0]] = args_tmp[1]

    return arg_dict

# generalized function for checking and assigning args
# By SL
def args_check(dict_default, dict_check):
    if len(dict_check):
        keys_missing = []

        # iterate through possible key vals in dict_check
        for key, val in dict_check.iteritems():
            # check to see if the possible keys are in dict_default
            if key in dict_default.keys():
                # assign the key/val pair in place
                # this operation acts IN PLACE on the supplied dict_default!!
                # therefore, no return value necessary
                dict_default[key] = ast.literal_eval(val)

            else:
                keys_missing.append(key)

        # if there are any keys missing
        if keys_missing:
            print "Options were not recognized: "
            fio.prettyprint(keys_missing)

# debug test function
if __name__ == '__main__':
    fparam = 'subject.param'
    p = read(fparam)
    print p
