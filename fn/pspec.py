# pspec.py - Plotting methods for spectral data.
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (All routines by MS)


# Standard Modules
import os
import matplotlib.pyplot as plt

# Local Modules
import specfn
import tsfn
import axes_create as ac

# Plot spectrogram with time series on same figure
# By MS
def pspec_ts(f_spec, f_ts, dfig, opts):
    # Load spec data from file
    spec = specfn.Spec(f_spec)

    # Load ts data from file
    ts = tsfn.TimeSeries(f_ts, opts['dtype'])

    # Generate file prefix 
    fprefix = f_spec.split('/')[-1].split('.')[0]

    # using png for now
    fig_name = os.path.join(dfig, fprefix+'.png')

    # f.f is the figure handle!
    if opts['ptype'] == 'spec':
        f = ac.FigSpec()

    elif opts['ptype'] == 'spec_with_welch':
        f = ac.FigSpecWelch()

    # Plot spectral data
    im = spec.plot_TFR(f.ax['spec'], xlim=opts['xlim'], ylim=[1, 60]) 
    f.f.colorbar(im, ax=f.ax['spec'])

    # Plot ts data
    ts.plot(f.ax['dipole'], opts['xlim'])

    # if welch indicated, plot:
    if opts['ptype'] == 'spec_with_welch':
        spec.plot_welch(f.ax['pgram'])

    # axis labels
    f.ax['spec'].set_xlabel('Time (ms)')
    f.ax['spec'].set_ylabel('Frequency (Hz)')
    f.ax['dipole'].set_ylabel('Dipole (nAm)')

    # use our fig classes to save and close
    f.savepng(fig_name)
    f.close()

# Plot spectral density
# By MS
def pspecpwr(file_name, density, error_vec=[]):
    # instantiate fig
    f = ac.FigStd()

    # Plot
    pspecpwr_ax(f.ax['ax0'], density)

    # Add error bars if necessary
    if len(error_vec):
        pyerrorbars_ax(f.ax['ax0'], density['f'], density['density'], error_vec)

    # axes labels
    f.set_notation_scientific(['ax0'])
    f.ax['ax0'].set_xlabel('Freq (Hz)')
    f.ax['ax0'].set_ylabel('Avgerage Power (nAm^2)')

    # Save fig
    f.save(file_name)
    f.close()

# frequency-power analysis plotting kernel
# By MS
def pspecpwr_ax(ax_specpwr, density):
    ax_specpwr.plot(density['f'], density['density'])

# Plot vertical error bars
# By MS
def pyerrorbars_ax(ax, x, y, yerr_vec):
    ax.errorbar(x, y, xerr=None, yerr=yerr_vec, fmt=None, ecolor='blue')
