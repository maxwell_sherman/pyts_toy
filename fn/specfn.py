# specfn.py - Average time-frequency energy representation using Morlet wavelet method
#
# 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (All routines by MS)

import os
import sys
import numpy as np
import scipy.signal as sps
import itertools as it
import matplotlib.pyplot as plt
import paramrw
import fileio as fio
import multiprocessing as mp

import tsfn
import fileio as fio
import axes_create as ac

# Class for manipulating spectral data
# Class by MS
class Spec():
    def __init__(self, fspec, dtype='ts'):
        # save dtype
        self.dtype = dtype

        # save details of file
        # may be better ways of doing this...
        self.fspec = fspec
        self.expmt = fspec.split('/')[6].split('.')[0]
        self.fname = fspec.split('/')[-1].split('-spec')[0]

        # parse data
        self.__parse_f(fspec)

    def __parse_f(self, fspec):
        data_spec = np.load(fspec)

        if self.dtype == 'ts':
            self.spec = {}

            # Try to load aggregate spec data
            try:
                self.spec['agg'] = {
                    't': data_spec['tvec'],
                    'f': data_spec['fvec'],
                    'TFR': data_spec['TFR'],
                }

            except KeyError:
                # Try loading aggregate spec data using old keys
                try:
                    self.spec['agg'] = {
                        't': data_spec['time'],
                        'f': data_spec['freq'],
                        'TFR': data_spec['TFR'],
                    }
                except KeyError:
                    print "No aggregate spec data found. Don't use fns that require it..."

    # Truncate t, f, and TFR for a specific layer over specified t and f intervals
    # Be warned: MODIFIES THE CLASS INTERNALLY
    def truncate(self, layer, t_interval, f_interval):
        self.spec[layer] = self.truncate_ext(layer, t_interval, f_interval)

    # Truncate t, f, and TFR for a specific layer over specified t and f intervals
    # Only returns truncated values. DOES NOT MODIFY THE CLASS INTERNALLY
    def truncate_ext(self, layer, t_interval, f_interval):
        # set f_max and f_min
        if f_interval is None:
            f_min = self.spec[layer]['f'][0]
            f_max = self.spec[layer]['f'][-1]

        else:
            f_min, f_max = f_interval

        # create an f_mask for the bounds of f, inclusive
        f_mask = (self.spec[layer]['f']>=f_min) & (self.spec[layer]['f']<=f_max)

        # do the same for t
        if t_interval is None:
            t_min = self.spec[layer]['t'][0]
            t_max = self.spec[layer]['t'][-1]

        else:
            t_min, t_max = t_interval

        t_mask = (self.spec[layer]['t']>=t_min) & (self.spec[layer]['t']<=t_max)

        # use the masks truncate these appropriately
        TFR_fcut = self.spec[layer]['TFR'][f_mask, :]
        TFR_tfcut = TFR_fcut[:, t_mask]

        f_fcut = self.spec[layer]['f'][f_mask]
        t_tcut = self.spec[layer]['t'][t_mask]
         
        return {
            't': t_tcut, 
            'f': f_fcut, 
            'TFR': TFR_tfcut,
        }

    # Averages spectral power over specified time interval for specified frequencies 
    def stationary_avg(self, layer='agg', t_interval=None, f_interval=None):
        # truncate data based on specified intervals
        dcut = self.truncate_ext(layer, t_interval, f_interval)

        # avg TFR pwr over time
        # axis = 1 sums over columns
        pwr_avg = dcut['TFR'].sum(axis=1) / len(dcut['t'])

        # Get max pwr and freq at which max pwr occurs
        pwr_max = pwr_avg.max()
        f_at_max = dcut['f'][pwr_avg==pwr_max]

        return {
            'p_avg': pwr_avg,
            'p_max': pwr_max,
            'f_max': f_at_max[0],
            'freq': dcut['f'],
            'expmt': self.expmt,
        }

    def plot_TFR(self, ax, layer='agg', xlim=None, ylim=None):
        # truncate data based on specifed xlim and ylim
        # xlim is a time interval
        # ylim is a frequency interval
        dcut = self.truncate_ext(layer, xlim, ylim)

        # Update xlim to have values guaranteed to exist
        xlim_new = (dcut['t'][0], dcut['t'][-1])
        xmin, xmax = xlim_new

        # Update ylim to have values guaranteed to exist
        ylim_new = (dcut['f'][0], dcut['f'][-1])
        ymin, ymax = ylim_new

        # set extent of plot
        # order is ymax, ymin so y-axis is inverted
        extent_xy = [xmin, xmax, ymax, ymin]

        # plot
        im = ax.imshow(dcut['TFR'], extent=extent_xy, aspect='auto', origin='upper')

        return im

# Class for performing Morlet Wavelet Spectral Analysis
# Algorithm adapted from FieldTrip Matlab Project (http://www.fieldtriptoolbox.org/)
# By MS
class MorletSpec():
    def __init__(self, tvec, tsvec, fparam, f_max=None, t_interval=None):

        # Get time and time-series data
        # Truncate if necessary
        if t_interval:
            self.tvec, self.tsvec = self.__truncate(t_interval, tvec, tsvec)

        else:
            self.tvec = tvec
            self.tsvec = tsvec

        # function is called this way because paramrw.read() returns 2 outputs
        self.p_dict = paramrw.read(fparam)

        # maximum frequency of analysis
        # Add 1 to ensure analysis is inclusive of maximum frequency
        if not f_max:
            self.f_max = self.p_dict['f_max_spec'] + 1
        else:
            self.f_max = f_max + 1

        # Array of frequencies over which to sort
        self.f = np.arange(1., self.f_max)

        # Number of cycles in wavelet (>5 advisable)
        self.width = 7.

        # Calculate sampling frequency
        self.fs = self.p_dict['sfreq']

        # Generate Spec data
        self.TFR = self.__traces2TFR()

    # externally callable save function
    def save(self, fdata_spec):
        write(fdata_spec, self.timevec, self.freqvec, self.TFR)

    def __truncate(self, t_interval, tvec, tsvec):
        # Get tmin, tmax from t_interval
        tmin, tmax = t_interval

        # Create mask
        tmask = (tvec >= tmin) & (tvec <= tmax)

        # Truncate vectors
        t_cut = tvec[tmask]
        ts_cut = tsvec[tmask]

        return t_cut, ts_cut

    # also creates self.timevec
    def __traces2TFR(self):
        self.S_trans = self.tsvec.transpose()

        # preallocation
        B = np.zeros((len(self.f), len(self.S_trans)))

        if self.S_trans.ndim == 1:
            for j in range(0, len(self.f)):
                s = sps.detrend(self.S_trans[:])
                B[j, :] += self.__energyvec(self.f[j], s)

            return B

        else:
            for i in range(0, self.S_trans.shape[0]):
                for j in range(0, len(self.f)):
                    s = sps.detrend(self.S_trans[i,:])
                    B[j,:] += self.__energyvec(self.f[j], s)

    def __energyvec(self, f, s):
        # Return an array containing the energy as function of time for freq f
        # The energy is calculated using Morlet's wavelets
        # f: frequency 
        # s: signal
        dt = 1. / self.fs
        sf = f / self.width
        st = 1. / (2. * np.pi * sf)

        t = np.arange(-3.5*st, 3.5*st, dt)
        m = self.__morlet(f, t)
        y = sps.fftconvolve(s, m)
        y = (2. * abs(y) / self.fs)**2
        y = y[np.ceil(len(m)/2.):len(y)-np.floor(len(m)/2.)+1]

        return y

    def __morlet(self, f, t):
        # Morlet's wavelet for frequency f and time t
        # Wavelet normalized so total energy is 1
        # f: specific frequency
        # t: not entirely sure...

        sf = f / self.width
        st = 1. / (2. * np.pi * sf)
        A = 1. / (st * np.sqrt(2.*np.pi))

        y = A * np.exp(-t**2. / (2. * st**2.)) * np.exp(1.j * 2. * np.pi * f * t)

        return y

    def __lnr50(self, s):
        # Line noise reduction (50 Hz) the amplitude and phase of the line notch is estimate.
        # A sinusoid with these characterisitics is then subtracted from the signal.
        # s: signal

        fNoise = 50.
        tv = np.arange(0,len(s))/self.fs

        if np.ndim(s) == 1:
            Sc = np.zeros(s.shape)
            Sft = self.__ft(s[:], fNoise)
            Sc[:] = s[:] - abs(Sft) * np.cos(2. * np.pi * fNoise * tv - np.angle(Sft))

            return Sc

        else:
            s = s.transpose()
            Sc = np.zeros(s.shape)

            for k in range(0, len(s)):
                Sft = ft(s[k,:], fNoise)
                Sc[k,:] = s[k,:] - abs(Sft) * np.cos(2. * np.pi * fNoise * tv - np.angle(Sft))

            return Sc.tranpose()

    def __ft(self, s, f):
        tv = np.arange(0,len(s)) / self.fs
        tmp = np.exp(1.j*2. * np.pi * f * tv)
        S = 2 * sum(s * tmp) / len(s)

        return S

# Aggregates together all spectral data from files provided in list_spec
# By MS
def spec_agg(list_spec, t_interval=None, f_interval=None):
    # iterate over provided files
    for fspec in list_spec:
        # load spec data and truncate as appropriately
        spec = Spec(fspec)
        d_cut = spec.truncate_ext('agg', t_interval, f_interval) 

        # Try summing TFR data
        try:
            x_agg += d_cut['TFR']

        # Excepts if x_agg doesn't exist yet (first iteration of loop)
        except NameError:
            x_agg = d_cut['TFR']

    # Return TFR data structure
    # Note, time and frequency data is identical for all files
    return {
        't': d_cut['t'],
        'f': d_cut['f'],
        'TFR': x_agg,
    }

# Density estimation for average spectral data
# By MS
def stationary_agg(list_spec, t_interval=None, f_interval=None):
    n = len(list_spec)
    data = spec_agg(list_spec, t_interval, f_interval)

    d_stationary = data['TFR'].sum(axis=1) 
    d_stationary /= n

    p_max = d_stationary.max()
    f_max = data['f'][d_stationary == p_max]

    return {
        'f': data['f'],
        'density': d_stationary,
        'p_max': p_max,
        'f_max': f_max,
    }


# Kernel for spec analysis of dipole data
# necessary for parallelization
# By MS
def spec_kernel(fparam, fts, fspec, f_max, t_interval=None, dtype='dpl'): 
    ts = tsfn.TimeSeries(fts, dtype)

    # Generate spec results
    spec_agg = MorletSpec(ts.data['t'], ts.data['ts'], fparam, f_max, t_interval)

    # Save spec results
    np.savez_compressed(fspec, tvec=spec_agg.tvec, fvec=spec_agg.f, TFR=spec_agg.TFR)

# Averages a given set of spec files
# Assumes TFR arrays are the same size and tvecs are all identical
# By MS
def average(file_name, fspec_list):
    # Aggregate data from files into one array
    for fspec in fspec_list:
        spec = Spec(fspec)

        # initialize and use x_spec_agg
        if fspec is fspec_list[0]:
            x_spec_agg = spec.spec['agg']['TFR']

        else:
            x_spec_agg += spec.spec['agg']['TFR']

    # poor man's mean
    x_spec_agg /= len(fspec_list)

    # save average data
    np.savez_compressed(file_name, TFR=x_spec_agg, tvec=spec.spec['agg']['t'], fvec=spec.spec['agg']['f'])
