# fileio.py - general file input/output functions
#
# v 0.0.1
# rev 2015-10-05 (MS: Created for Center for Open Science)
# Notes: (Some functions by MS. Some by SL)

import os
import numpy as np
import paramrw

# Print any iterable
# By SL
def prettyprint(l):
    for item in l:
        print item

# Cleans input files
# By SL
def clean_lines(file):
    with open(file) as f_in:
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines if line]

    return lines

# Get the data files matching file_ext in this directory
# Not Recursive!
# By SL. Modified by MS
def file_match(dsearch, file_ext):
# def file_match(dsearch, file_ext, local=0):
    file_list = [os.path.join(dsearch, file) for file in os.listdir(dsearch) 
                 if file.endswith(file_ext)]

    # sort file list
    file_list.sort()

    return file_list

# Check directory for existence
# By SL
def dir_check(d):
    if not os.path.isdir(d):
        return 0

    else:
        return os.path.isdir(d)

# Creation directory
# only create if check comes back 0
# By SL
def dir_create(d):
    if not dir_check(d):
        os.makedirs(d)

# Build paths to directories to search using file_match
# By MS
def dir_to_search(ddata, opts):
    # ddata: top level data directory
    # opts: dictionary that includes sub-dir to search in and data type to search for
    # if name of sub-dir is supplied, construct full path to it
    if opts['dir']:
        f_dir = os.path.join(ddata, opts['dir'])

    # otherwise, search the whole data directory
    else:
        f_dir = ddata
        # f_dir = os.path.join(ddata, 'raw' + opts['dtype'])

    return f_dir

# Return the proper file extension for a given data type
# By MS
def get_file_ext(dtype):
    datatypes = {
        'dpl': '-dpl.txt',
        'spec': '-spec.npz',
        'lfp': '-lfp.npz',
        'lam': '-lam.npz',
        'dplavg': '-dplavg.txt',
        'tmpllam': '-tmpllam.txt',
        'tmpldpl': '-tmpldpl.txt',
        'tmpldplavg': 'tmpldplavg.txt',
        'tmpldplavgavg': 'tmpldplavgavg.txt',
    }

    # if dtype already has file ext, return it
    if dtype.endswith(('.txt', '.npz')):
        file_ext = dtype

    # otherwise find file_ext in datatypes dictionary
    else:
        file_ext = datatypes[dtype]

    return file_ext
