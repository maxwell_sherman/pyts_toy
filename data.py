#!/usr/bin/env python

# data.py - creates command line interface for PyTS (toy version)
#
# v 0.0.1
# rev 2015-10-05: (MS: Created for Center for Open Science)
# last major: Original script by SL. Modified by MS.

import os, readline, shutil, sys
from fn.cli import Console

if __name__ == '__main__':

    # Readline goodness.
    readline.parse_and_bind('set editing-mode vi')
    readline.parse_and_bind('\C-l: clear-screen')
    readline.parse_and_bind('tab: complete')

    # Local history file
    fhist_local = '.data_hist_local'

    # Set the readline history file to fhist_local
    readline.read_history_file(fhist_local)

    # Reverse history search: Apple
    if sys.platform.startswith('darwin'):
        readline.parse_and_bind('"^[[A": history-search-backward')
        readline.parse_and_bind('"^[[B": history-search-forward')

    # Reverse history search: Linux
    elif sys.platform.startswith('linux'):
        readline.parse_and_bind('"\e[A": history-search-backward')
        readline.parse_and_bind('"\e[B": history-search-forward')

    # Run the console
    console = Console()
    console.cmdloop()
