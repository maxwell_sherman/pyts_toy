% Tex source for user guide to PyTS Toy. Written a bit on the fly - sorry future.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[11pt]{article}

% Useful packages
\usepackage{fullpage}
\usepackage{fixltx2e}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{times}
\usepackage[colorlinks=true, linkcolor=blue, citecolor=blue, urlcolor=blue]{hyperref}
\usepackage[utf8]{inputenc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

% Title
  \title{PyTS Toy: A User Guide}
  \author{
    Maxwell A. Sherman\\
    \texttt{msherman997@gmail.com}
  }
  \date{\today}
  \maketitle

\tableofcontents

%% Introduction
\section{Introduction}
Hello and welcome to the Python Time Series analysis shell (PyTS). PyTS is a command line interface (CLI) package for the manipulation and analysis of time series data. This documentation supports a toy version of PyTS designed to give you a taste for the package's functionality and a flavor for my python development style. This guide will walk you through some exploratory time series analysis of non-invasively recorded data from the human neocortex. Please enjoy.

%% Authorship
\section{Authorship}
I developed PyTS in collaboration with Shane Lee, a postdoc in the Brown University Department of Neuroscience. While this toy version of the package is a highly modified version of the original, it still contains work by Shane. Indeed, Shane played an essential role in this project, both in contributing important routines to the package and in helping to inspire and guide my own work on the package. In order to properly credit Shane's contributions I have modified the code in two ways.
\begin{enumerate}
  \item Every file begins with a header providing the usual information of version number, date modified, and modification notes. To each header I have added a `Notes' section in which I state who contributed routines to that file. For example, if I wrote the entirety of a script, the header would state `All routines by MS'. If Shane contributed routines, the header may say `Routines by MS and SL'.
  \item Since several scripts contain functions by both me and Shane, I have additionally labeled every function and class in the package as either `By MS' or `By SL' in a comment directly above the function or class definition. If the function was originally written by Shane but later modified by me, the comment may read `By SL. Modified by MS.'
\end{enumerate}

%% Requirements
\section{Requirements}
PyTS runs within a terminal and requires the following dependencies: python 2.7, and recent versions of NumPy, SciPy, matplotlib, and mpi4Py.

%% Data Directory Structure
\section{Data Directory Structure}
The full version of PyTS is not yet freely available to the scientific community as it is being beta-tested for a scientific publication currently under review. It is relatively flexible in adapting to an individual user's data structure. The one requirement is that each directory of raw data contains a flat text file ending \texttt{.param} with relevant meta data such as sampling frequency. For ease of use in this guide, I have included a data directory within the source code.

%% Package Structure
\section{Package Structure}

  % Data.py
\subsection{data.py}
  {\tt data.py} is the topmost python file in the package. It is the executable that starts the CLI. It adds useful functionality to the CLI including tab completion and history search.

  % fn/cli.py
\subsection{fn/cli.py}
  {\tt fn/cli.py} contains the routines that establish the CLI and contains all functions executable from within the CLI. Callable functions are defined as {\tt def do\_<function-name>}. Such functions are executed via the CLI by typing
  \begin{verbatim}
    [datash] <function-name> --option1=value1 --option2=value2
  \end{verbatim}
where options are additional arguments that can be passed to the function being executed. There is no limit to the number of options a CLI function can take.

  % fn/clidefs.py 
\subsection{fn/clidefs.py}
  All executable functions of the form {\tt do\_<function-name>} in {\tt fn/cli.py} are wrappers that call a function within {\tt fn/clidefs.py} usually defined as {\tt def exec\_<function-name>}. Functions within {\tt fn/clidefs.py} generally handle file input/output and pass these files onto data / task-specific functions contained in other files.

  % Other files in /fn
\subsection{Other files in /fn}
    \begin{itemize}
      % tsfn.py
      \item {\tt tsfn.py}: functions for manipulating time-series data. Contains {\tt Class TimeSeries}, which handles most manipulations of raw time series data.
      % specfn.py
      \item{\tt specfn.py}: functions for generating and manipulating spectral data.
      % axes_create.py
      \item {\tt axes\_create.py}: classes that establish axes for plotting. Essential for almost all plotting routines in the CLI.
      % paramrw.py
      \item {\tt paramrw.py}: functions to handle metadata as well as function options passed into the CLI.
      % fileio.py
      \item {\tt fileio.py}: functions to support file input / output.
    \end{itemize}

%% Usage
\section{Usage: By Example}
  We now outline some functionality of this toy version of PyTS by walking you through the some data analysis. Please feel free to play along in your own terminal.

  % Starting the CLI and Loading Data
\subsection{Starting the CLI and Loading Data}
  To start the CLI, open a terminal, browse into the pyts\_toy directory and type
  \begin{verbatim}
    $ python data.py
  \end{verbatim}
When the CLI starts, you will be greeted by the following
  \begin{verbatim}
    This is the time-series analysis Shell

    Data directory is: /path/to/source/pyts_toy/data
    [datash]
  \end{verbatim}
{\tt [datash]} is the user input prompt, just like {\tt >>>} in a python terminal or {\tt \$} in a Bash terminal. 

Let us now pretend that we are a scientist wishing to perform some exploratory analysis on some experimental data. Presumably, we are a prolific scientist who has data from multiple experiments within our data directory, and we would like to browse into a particular one. However, perhaps we are also a forgetful scientist, and cannot remember what experiments are in this particular directory. We can remind ourselves by listing the contents of our current working directory just as in a Bash terminal. The command and resulting output are:
  \begin{verbatim}
     [datash] ls
     /expmt_01
     /expmt_02
  \end{verbatim}

Let us browse into the creatively named {\tt expmt\_01} directory. To do this, we use the {\tt setexpt} function. Again, command and resulting output are as follows:
  \begin{verbatim}
    [datash] setexpt expmt_01
    Experiment set to /path/to/source/directory/data/expmt_01
  \end{verbatim}

Listing the contents of this experiment directory, we see this experiment contains data from two subjects.
  \begin{verbatim}
     [datash] ls
     /subject_01
     /subject_02
  \end{verbatim}

  To access the data from {\tt subject\_01} within our experiment directory, we use the {\tt load} command, which works as:
  \begin{verbatim}
    [datash] \load subject_01
    Trial directory set to: /path/to/source/directory/data/expmt_01/trial_01.
    Ready for data analysis.
  \end{verbatim}
  Listing the contents of this directory we notice it has the directory {\tt /rawts} (which contains raw time series data) and the file {\tt subject.param} (which is our relevant metadata file).

% Some Exploratory Plotting
\subsection{Some Exploratory Plotting}
Curious to see what our data actually look like, we proceed to plot the raw time series. We do this as:
  \begin{verbatim}
    [datash] plot --ptype='ts' --dir='rawts' --dtype='dpl'
  \end{verbatim}

The CLI will output a file path. This is path to the directory in which the plots are saved. If such a directory does not previously  exist, PyTS creates one. Opening our data directory, we notice a new directory {\tt figts} has been created, and that there are ten .png files inside. These are plots of our raw data.

A point of note: to speed up redundant tasks, PyTS parallelizes tasks whenever possible. The plotting of multiple files is thus distributed across all available processors. This can make debugging difficult at times, so to force serial plotting, we can pass the {\tt --runtype=`debug'} option to the function.

% Spectral Analysis
\subsection{Spectral Analysis}
Looking through the resulting plots of our raw data, we notice the time series seem to oscillate quite rhythmically. This leads us to think that perhaps looking at the frequency content of our data would provide useful insights. PyTS includes functions to perform a Morlet wavelet spectral analysis, for example. To carry out this analysis, we type
  \begin{verbatim}
    [datash] spec_analysis --dir_in='rawts' --dir_out='rawspec' 
             --dtype='dpl'
  \end{verbatim}
Again, PyTS outputs the pathway to the directory in which the spectral analysis files will be saved. Browsing into this directory there should be ten .npz files, which contain the spectral information from each of the ten raw time series data files.

To plot the raw spectral data, we again use the plot function:
  \begin{verbatim}
    [datash] plot --ptype='spec' --dtype='dpl' --dir='rawspec' 
             --dir_ts='rawts'
  \end{verbatim}
As usual, the CLI will output the path to the directory in which the plots are saved. In this case, {\tt figspec}.

% Average Spectral Analysis
\subsection{Average Spectral Analysis}
Looking through the resulting spectrograms, we notice that there appears to be strong \--- if transient \--- activity in the 10-20 Hz band. To see see if there is any pattern in this activity, we can average together the raw time series and spectral data. To do this, we type
  \begin{verbatim}
    [datash] average --dtype='dpl' --dir='rawts'

    [datash] average --dtype='spec' --dir='rawspec'
  \end{verbatim}

To plot the resulting files, we again use the plot function
  \begin{verbatim}
    [datash] plot --ptype= 'ts' --dtype='dpl' --dir='avgts'

    [datash] plot --ptype='spec' --dtype='dpl' --dir='avgspec'
             --dir_ts='avgts'
  \end{verbatim}

Looking at the average spectrogram, we note that there appears to be 10-20 Hz activity occurring broadly across all time. Therefore, a time-frequency representation may not be the most appropriate presentation of the data. Rather, we can remove the time dimension by performing a spectral density estimation.

  \begin{verbatim}
    [datash] density --dir='rawspec'
  \end{verbatim}

Opening the resulting density plot, we see that there is a large peak at 10 Hz which drops off to a smaller peak at 20 Hz. This is a general feature of all time series signals recorded non-invasively from the human brain and has spawned and entire area field of neuroscience research dedicated to understanding where these 10-20 Hz rhythms arise from and what function they may play in cognition.

% Additional Functions
\section{Getting Help}
  To see all functions available through the CLI type
  \begin{verbatim}
    [datash] help
  \end{verbatim}

  To see documentation for a specific function, type
  \begin{verbatim}
    [datash] help <function-name>
  \end{verbatim}

 If you have any other questions, feel free to contact me via email at {\tt msherman997@gmail.com} or by phone at (781) 820-0973.

\end{document}
